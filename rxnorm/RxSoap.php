<html>
<head/>
<body>
<?php
    
  $wsdl = "http://rxnav.nlm.nih.gov/NdfrtAPI.xml";
    
  $client = new SoapClient($wsdl, array("trace"=>1, "exceptions"=>0));
	$nui = array();
	$count = 0;
	$RXCUIPost = $_POST["RXCUI"];
	
	$count = 0;
	//RXCUI itself is an array
	foreach ($RXCUIPost as $value) {
		$test = $client->findConceptsByID( "RXCUI", "$value" );
		$holder = $test[1];
		$drug_temp = $holder->NAME;
		$nui_temp = $holder->NUI;
		$nui[$count] = $nui_temp;
		$count++;
	}
	
	$data = $client->findInteractionsFromList($nui, 3);
	$num = count($data);
	print "<pre>\n";
	echo "There were a total of $num drug interactions found.\n";
	echo "<br />\n";
	//outer for loop is over the number of severities
	for($counter=0; $counter<3; $counter++)
	{
		if ($counter==0)
			$severity_test = 'Critical';
		if ($counter==1)
			$severity_test = 'Significant';
		for($i=0; $i<$num; $i++)
		{
			$xml = $data[$i];
			$interaction_array = $xml->interactionTriple;
			$interaction_one = $interaction_array[0];
			$drug_one_a = $interaction_one->drug1->NAME;
			$drug_two_a = $interaction_one->drug2->NAME;
			$severity_a = $interaction_one->severity;
			if($severity_test == $severity_a && $counter < 2) {
				echo "Drug Interaction Identified between $drug_one_a and $drug_two_a of severity $severity_a .\n";
				echo "<br />\n";
			}
			else if ($counter == 2 && $severity_a != 'Critical' && $severity_a != 'Significant'){
				echo "Drug Interaction Identified between $drug_one_a and $drug_two_a of severity $severity_a .\n";
				echo "<br />\n";
			}
		}
	}
	
	//Send GET request filled with the data array to the display.php page
	$getDataRequest = $_POST[data];
	
	echo "<br />\n";
	print "</pre>";
?>