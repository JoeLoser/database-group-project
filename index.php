<html>
	<head>
		<title>Database Final Project - Group 17</title>	

		<link rel="icon" type="image/png" href="img/favicon.png">
			
		<script type="text/javascript" src="js/jquery-1.10.2.js"></script>		
		<script type="text/javascript" src="js/jquery-ui-1.10.4.custom.min.js"></script>		

		<link rel="stylesheet" type="text/css" href="css/jquery-ui-1.10.4.custom.min.css">
		<link rel="stylesheet" type="text/css" href="css/style.css">

		<script type="text/javascript" >
			$(document).ready(function(){
				
				$( document ).tooltip({
					position:{ 
						my: "center top+5", 
						at: "center bottom",
						collision: "flipfit" 
					},
					show:{effect: "slideDown"},
					tooltipClass: "tooltip"
				});
			 
		   	$("#searchbox").autocomplete({
		     		source: "lib/autocompletenew.php",
		     		minLength: 1
			  	});
			  	
			  	$('#search_form').submit(function(e) {
			  		e.preventDefault();
			  		
			  		var value=$("#searchbox").val();
                    var generic = value.substring(0, value.indexOf(" ("));			//Get value from search box
					var brand = value.substring(value.indexOf(" ("));    
                        
                    var cui;									//Will store the cui for the string if one is found
				
					//Make sure there is a value to return
					if(generic.length < 1)
						return;				
					
					var cui = 0;
					var titleStr = "";							
				
					if(generic.length > 15)
						titleStr = "title=\"" + generic + brand +  "\"";

					//Post to ajax to validate the string and get cui number
					$.ajax({url:"lib/validateCui.php?str=" + generic, success:function(result){
							if(result.match(/^ERROR/gi)){
								alert(result);
								return;						
							}

                            var dupe = false;
                            $("#drug_list").children().each(function(){
                                if(result == $(this).attr("rxcui")){
                                    dupe = true;
                                }
                            }); 

                            if(dupe)
                            {
                                alert("Duplicate entries for \"" + generic + brand +  "\" with rxcui of \"" + result + "\"" );
                                return;
                            }

							var cui = result;		//Result is what we get back from the validateCui script
							var titleStr = "";							
						
							if(value.length > 15)
								titleStr = "title=\"" + generic + brand + "\"";

							$("#drug_list").append("<li class=\"drug_item round\" rxcui=\"" + cui + "\" " + titleStr + "><span onclick=\"deleteDrug(" + cui + ")\">&#10060</span>" + generic + brand + "<input type=\"hidden\" name=\"" + generic + brand + "\" value=\"" + cui + "\"></li>");
							$("#searchbox").val("");
					  }});	

				  });
	
	
			});
			
			function deleteDrug(cui){
				$("li[rxcui=" + cui + "]").fadeOut("fast", function(){
					$(this).remove();				
				});
			}
		</script>
	</head>
	
	<body>
		<div id="container">	
			<h1>Drug Interaction Database</h1>
	
			<form id="search_form" method="post">
				<input id="searchbox" class="round" type="text" placeholder="Enter drug name...">
				<input class="round" type="submit" value="Add">
			</form>
			
			<form id="drug_form" method="POST" action="display.php">
				<ul id="drug_list">

				</ul>	
			
			    <input id="submit_drugs" type="submit" class="round" value="Submit DRUGS">
			</form>
		</div>
		
		<footer>
			<p>CS3380 Final Project - Group 17</p>
			<p>Patrick Hennessy | CJ Voege | Beau Lavoie | Jaie Lavoie | Joe Loser | Noelan Hensley</p>
		</footer>		
	</body>
</html>
