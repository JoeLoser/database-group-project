<?php
/*		This class will make a simplier interface for this assignment
			the constructor will create a connection to the database
			the exec function will prepare the query and then execute while error checking
*/

	define('SQL_HOSTNAME','dbhost-mysql.cs.missouri.edu');
	define('SQL_USERNAME','cs3380s14grp17');
	define('SQL_PASSWORD','k_QM+9+DW3');
	define('SQL_DATABASE','cs3380s14grp17');

require_once("../../secure/database.php");

class mysql
{
	private $dbc;
	private $rows_affected;	
	
	function __construct()
	{
		$this->dbc = mysqli_connect(SQL_HOSTNAME, SQL_USERNAME, SQL_PASSWORD);
        mysqli_select_db($this->dbc, SQL_DATABASE);    
		
        $this->rows_affected = 0;
	}
	
	function test($query)
	{
		//return mysqli_query($this->dbc, $query);	

        $stmt = mysqli_prepare($this->dbc, $query);
        
        mysqli_stmt_bind_param($stmt, "s", $arg1);
        
        $arg1 = 'Heparin';

        mysqli_stmt_execute($stmt);

        return $stmt;

	}	
	
	function exec($query, $values)
	{
		//Sanitize all the arguments
		foreach ($values as $v)
		{
			$v = htmlspecialchars($v);
		}	
	
		//Prepare the statement
		$result = mysqli_prepare($this->dbc, $query);
		
		//Check for errors
		if(!$result)		
			return false;
		
		//Attempt execution
		//$result = mysqli_execute($this->dbc, "", $values);
		$result = mysqli_execute($result);
		//Check errors
		if(!$result)
			return false;

		$this->rows_affected = mysqli_affected_rows($result);		
		
		//If all went well, we return the result from the query 
		return $result;	
	}
	
	function getAffectedRows()
	{
		return $this->rows_affected;	
	}
	
	function getErrors()
	{
		return mysqli_error($this->dbc);		
	}
	
	function close()
	{
		mysqli_close($this->dbc);
	}
}
?>
